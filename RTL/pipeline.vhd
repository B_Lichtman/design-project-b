library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity pipeline is
	Port (
		clk			: in std_logic;
		point_in	: in point;
		point_out	: out point
	);
end pipeline;

architecture arch of pipeline is
	signal buf		: std_logic;

	signal pixel_base	: integer;

	-- 127 is subtracted from each value before loading to turned them into signed values around 0
	-- Buffers should be loaded in column major
	signal buf0	: dmatrix;
	signal buf1	: dmatrix;

	signal processing_matrix	: dmatrix;
	signal transformed_matrix	: dmatrix;
	signal quantised_matrix		: dmatrix;
begin

	-- Selectors and parameters

	-- For double-buffering
	processing_matrix <= buf0 when buf = '0' else buf1;

	TIMING:
	entity work.timing port map(
		clk			=> clk,

		buf			=> buf,

		pixel_base	=> pixel_base
	);

	-- PIPELINE STAGE 1 - LOADING

	LOADER:
	entity work.loader
	generic map(
		offset => 0
	)
	port map(
		clk			=> clk,
		pixel_base	=> pixel_base,
		buf			=> buf,
		point_in	=> point_in,

		buf0	=> buf0,
		buf1	=> buf1
	);

	-- PIPELINE STAGE 2 - DCT

	DCT:
	entity work.dct
	generic map(
		offset	=> 0
	)
	port map(
		clk			=> clk,
		pixel_base	=> pixel_base,
		matrix_in	=> processing_matrix,

		o			=> transformed_matrix
	);

	-- PIPELINE STAGE 3 - QUANTISATION

	QUANTISE:
	entity work.quantise
	generic map(
		offset	=> 21
	)
	port map(
		clk			=> clk,
		pixel_base	=> pixel_base,
		matrix_in	=> transformed_matrix,

		o			=> quantised_matrix
	);

	-- PIPELINE STAGE 4 - OUTPUT

	OUTP:
	entity work.outp
	generic map(
		offset	=> 22
	)
	port map(
		clk			=> clk,
		pixel_base	=> pixel_base,
		matrix_in	=> quantised_matrix,

		o			=> point_out
	);

end arch;
