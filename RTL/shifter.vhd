library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity shifter is
	Generic (
		shifts		: integer
	);
	Port (
		clk			: in std_logic;
		a			: in dpoint;
		b			: in dpoint;

		o			: out dpoint := (others => '0')
	);
end shifter;

architecture arch of shifter is
	signal sign_extend_a	: dpoint;
	signal sign_extend_b	: dpoint;

begin
	--sign_extend_a <= dpoint(resize(signed(a), 2 * point_depth));
	--sign_extend_b <= dpoint(resize(signed(b), 2 * point_depth));

	process(clk) is
	begin
		if (rising_edge(clk)) then
			if a(shifts) = '0' then
				o <= (others => '0');
			else
				o <= dpoint(shift_left(signed(b), shifts));
			end if;
		end if;
	end process;

end arch;
