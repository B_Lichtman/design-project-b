library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity convolve is
	Port (
		clk			: in std_logic;
		line_left	: in dline;
		line_right	: in dline;

		o			: out dpoint
	);
end convolve;

architecture arch of convolve is
	signal c_sig	: c_tree;
begin

	-- One multiplier for each number in a line
	GEN_MULTIPLIERS:
	for I in 0 to block_size - 1 generate
	begin
		multiplier:
		entity work.multiplier port map(
			clk	=> clk,
			a	=> line_left(i),
			b	=> line_right(i),
			o	=> c_sig(block_size + I)
		);
	end generate;

	-- Tree of adders starting at index 1
	GEN_ADDERS:
	for I in 1 to block_size - 1 generate
	begin
		d_adder:
		entity work.double_adder port map(
			clk	=> clk,
			a	=> c_sig(2 * I),
			b	=> c_sig(2 * I + 1),
			o	=> c_sig(I)
		);
	end generate;

	process(clk) is
	begin
		if (rising_edge(clk)) then
			o <= c_sig(1);
		end if;
	end process;
end arch;
