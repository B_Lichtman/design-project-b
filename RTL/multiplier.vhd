library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity multiplier is
	Port (
		clk		: in std_logic;
		a		: in dpoint;
		b		: in dpoint;

		o		: out dpoint
	);
end multiplier;

architecture arch of multiplier is
	signal m_sig	: m_tree;
begin

	GEN_SHIFTERS:
	for I in 0 to 2 * point_depth - 1 generate
	begin
	    shift:
		entity work.shifter
			generic map(
				shifts	=> I
			)
			port map(
				clk	=> clk,
				a	=> a,
				b	=> b,
				o	=> m_sig(2 * point_depth + I)
			);
	end generate;

	GEN_ADDERS:
	for I in 1 to 2 * point_depth - 1 generate
	begin
	    d_adder:
		entity work.double_adder port map(
			clk	=> clk,
			a	=> m_sig(2 * I),
			b	=> m_sig(2 * I + 1),
			o	=> m_sig(I)
		);
	end generate;

	process(clk) is
	begin
		if (rising_edge(clk)) then
			o <= m_sig(1);
		end if;
	end process;
end arch;
