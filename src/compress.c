#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BLOCK_SIZE 8
#define WIDTH 256
#define HEIGHT 256
#define SIZE WIDTH * HEIGHT

// Image vectors
// +x is "right" on the image
// +y is "down" on the image
struct imgvec
{
	int x;
	int y;
};

static struct imgvec ivneg(struct imgvec v);
static struct imgvec ivadd(struct imgvec a, struct imgvec b);
static struct imgvec offset2iv(int offset, int width);
static int iv2offset(struct imgvec v, int width);

static const float DCT[64] =
{
	0.353553, 0.353553, 0.353553, 0.353553, 0.353553, 0.353553, 0.353553, 0.353553,
	0.490393, 0.415735, 0.277785, 0.097545, -0.097545, -0.277785, -0.415735, -0.490393,
	0.461940, 0.191342, -0.191342, -0.461940, -0.461940, -0.191342, 0.191342, 0.461940,
	0.415735, -0.097545, -0.490393, -0.277785, 0.277785, 0.490393, 0.097545, -0.415735,
	0.353553, -0.353553, -0.353553, 0.353553, 0.353553, -0.353553, -0.353553, 0.353553,
	0.277785, -0.490393, 0.097545, 0.415735, -0.415735, -0.097545, 0.490393, -0.277785,
	0.191342, -0.461940, 0.461940, -0.191342, -0.191342, 0.461940, -0.461940, 0.191342,
	0.097545, -0.277785, 0.415735, -0.490393, 0.490393, -0.415735, 0.277785, -0.097545
};

static const float IDCT[64] =
{
	0.353553, 0.490393, 0.461940, 0.415735, 0.353553, 0.277785, 0.191342, 0.097545,
	0.353553, 0.415735, 0.191342, -0.097545, -0.353553, -0.490393, -0.461940, -0.277785,
	0.353553, 0.277785, -0.191342, -0.490393, -0.353553, 0.097545, 0.461940, 0.415735,
	0.353553, 0.097545, -0.461940, -0.277785, 0.353553, 0.415735, -0.191342, -0.490393,
	0.353553, -0.097545, -0.461940, 0.277785, 0.353553, -0.415735, -0.191342, 0.490393,
	0.353553, -0.277785, -0.191342, 0.490393, -0.353553, -0.097545, 0.461940, -0.415735,
	0.353553, -0.415735, 0.191342, 0.097545, -0.353553, 0.490393, -0.461940, 0.277785,
	0.353553, -0.490393, 0.461940, -0.415735, 0.353553, -0.277785, 0.191342, -0.097545
};

int matrix_multiply(float * final, float * a, float * b, int size)
{
	struct imgvec out;
	for(out.y = 0; out.y < size; out.y++)
	{
		for(out.x = 0; out.x < size; out.x++)
		{
			final[iv2offset(out, BLOCK_SIZE)] = 0;
			// Using current pixel, iterate multiplies
			struct imgvec in_a = {0, out.y};
			struct imgvec in_b = {out.x, 0};
			for(int i = 0; i < size; i++)
			{
				in_a.x = i;
				in_b.y = i;
				// printf("Multiplying: %d, %d with %d, %d into %d, %d\n", in_a.x, in_a.y, in_b.x, in_b.y, out.x, out.y);
				final[iv2offset(out, BLOCK_SIZE)] += a[iv2offset(in_a, BLOCK_SIZE)] * b[iv2offset(in_b, BLOCK_SIZE)];
			}
		}
	}
	return 0;
}

int dct(float * grid)
{
	// Perform DCT and copy back into orignal grid
	float intermediate_grid[64];
	matrix_multiply(intermediate_grid, (float *)DCT, grid, BLOCK_SIZE);
	matrix_multiply(grid, intermediate_grid, (float *)IDCT, BLOCK_SIZE);
	return 0;
}

int idct(float * grid)
{
	// Perform Inverse DCT and copy back into orignal grid
	float intermediate_grid[64];
	matrix_multiply(intermediate_grid, (float *)IDCT, grid, BLOCK_SIZE);
	matrix_multiply(grid, intermediate_grid, (float *)DCT, BLOCK_SIZE);
	return 0;
}

int get_grid(float * grid, void * mem, struct imgvec corner)
{
	// For each 8x8 top left pixel, get the 8x8 grid of corresponding pixels
	struct imgvec offset;
	for(offset.y = 0; offset.y < 8; offset.y++)
	{
		for(offset.x = 0; offset.x < 8; offset.x++)
		{
			struct imgvec coord = ivadd(corner, offset);
			int memoffset = iv2offset(coord, WIDTH);
			int gridoffset = iv2offset(offset, 8);
			grid[gridoffset] = (float)((u_int8_t *)mem)[memoffset];
		}
	}
	return 0;
}

int write_grid(float * grid, void * mem, struct imgvec corner)
{
	// For each 8x8 top left pixel, write the 8x8 grid of corresponding pixels to file
	struct imgvec offset;
	for(offset.y = 0; offset.y < 8; offset.y++)
	{
		for(offset.x = 0; offset.x < 8; offset.x++)
		{
			struct imgvec coord = ivadd(corner, offset);
			int memoffset = iv2offset(coord, WIDTH);
			int gridoffset = iv2offset(offset, 8);
			((u_int8_t *)mem)[memoffset] = (u_int8_t)grid[gridoffset];
		}
	}
	return 0;
}

int compress(int r, int w)
{
	// Map read and write files into memory
	void * readmem = mmap(NULL, SIZE, PROT_READ, MAP_SHARED, r, 0);
	void * writemem = mmap(NULL, SIZE, PROT_WRITE, MAP_SHARED, w, 0);
	if(readmem == MAP_FAILED || writemem == MAP_FAILED)
	{
		return 1;
	}

	float current_grid[64];

	// Loop through the top-left pixels of each 8x8 grid
	struct imgvec cur;
	for(cur.y = 0; cur.y < WIDTH; cur.y += 8)
	{
		for(cur.x = 0; cur.x < HEIGHT; cur.x += 8)
		{
			// GET GRID
			get_grid(current_grid, readmem, cur);

			// DO DCT
			dct(current_grid);

			// QUANTISE

			// INVERSE DCT
			idct(current_grid);

			// WRITE GRID
			write_grid(current_grid, writemem, cur);
		}
	}

	int unmapread = munmap(readmem, SIZE);
	int unmapwrite = munmap(writemem, SIZE);
	if(unmapread < 0 || unmapwrite < 0)
	{
		return 2;
	}

	return 0;
}

int open_files(char * readfile, char * writefile, int * readhandle, int * writehandle, int mode)
{
	int r = open(readfile, O_RDONLY);
	int w = open(writefile, O_RDWR | O_CREAT | O_TRUNC, mode);
	if(r < 0 || w < 0)
	{
		printf("Could not open file/s\n");
		return 1;
	}

	// Write a null byte to resize the write file
	off_t seek = lseek(w, (size_t)(SIZE - 1), SEEK_SET);
	if(seek < 0)
	{
		printf("Fseek error\n");
		return 1;
	}
	write(w, "", 1);

	*readhandle = r;
	*writehandle = w;
	return 0;
}

int main(int argc, char ** argv)
{
	int err;

	if(argc < 3 || argc > 6)
	{
		printf("Usage: %s <input> <output> [width] [height] [pixel depth]\n", argv[0]);
		exit(1);
	}

	int width = 256;
	int height = 256;
	int depth = 3;

	if(argc >= 4)
	{
		width = atoi(argv[3]);
	}
	if(argc >= 5)
	{
		height = atoi(argv[4]);
	}
	if(argc >= 6)
	{
		depth = atoi(argv[5]);
	}

	int r = -1;
	int w = -1;

	err = open_files(argv[1], argv[2], &r, &w, 0x0644);
	if(err)
	{
		exit(1);
	}

	err = compress(r, w);
	if(err)
	{
		printf("Compression failed: %d\n", err);
		exit(1);
	}
}

static struct imgvec ivneg(struct imgvec v)
{
	struct imgvec res = {-v.x, -v.y};
	return res;
}

static struct imgvec ivadd(struct imgvec a, struct imgvec b)
{
	struct imgvec res = {a.x + b.x, a.y + b.y};
	return res;
}

static struct imgvec offset2iv(int offset, int width)
{
	struct imgvec res = {offset % width, offset / width};
	return res;
}

static int iv2offset(struct imgvec v, int width)
{
	return v.y * width + v.x;
}
