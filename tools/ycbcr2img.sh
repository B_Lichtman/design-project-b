#!/bin/sh
# Requires imagemagick
# Usage ./ycbcr2img.sh <dimensions> <input prefix> <output>
# Example: ./img2ycbcr.sh 256x256 split out.jpg

convert -size $1 -depth 8 -set colorspace YCbCr $2_*.gray -combine $3
