#!/bin/sh
# Requires imagemagick
# Usage: ./img2ycbcr.sh <input> <output prefix>
# Example: ./img2ycbcr.sh test.jpg split

convert $1 -colorspace YCbCr -depth 8 -separate $2_%d.gray
