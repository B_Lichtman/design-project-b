library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity double_adder is
	Port (
		clk		: in std_logic;
		a		: in dpoint;
		b		: in dpoint;

		o		: out dpoint
	);
end double_adder;

architecture arch of double_adder is
begin
	process(clk) is
	begin
		if (rising_edge(clk)) then
			o <= dpoint(unsigned(a) + unsigned(b));
		end if;
	end process;
end arch;
