library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dct_buffered is
	port (
		clk	: in std_logic;
		enable		: in std_logic;

		data_in : in STD_LOGIC_VECTOR (31 downto 0);
		ready	: out std_logic;

		data_out : out STD_LOGIC_VECTOR (31 downto 0);
		valid	: out std_logic
	);
end;


architecture arch of dct_buffered is
	constant pipeline_latency	: integer := 150;
	constant points_in_buffers	: integer := 2;
	constant point_depth	: integer := 16;

	signal clk_counter	: integer	:= 0;
	signal output_valid	: std_logic	:= '0';

	signal in_select	: integer	:= 0;
	signal out_select	: integer	:= 0;

	signal pipeline_in	: std_logic_vector(point_depth - 1 downto 0);
	signal pipeline_out	: std_logic_vector(point_depth - 1 downto 0);

begin

	process(clk) is
	begin
		if (rising_edge(clk)) then
			if (clk_counter + 1 = pipeline_latency) then
				output_valid <= '1';
			else
				clk_counter <= clk_counter + 1;
			end if;

			-- Input buffer
			if (in_select + 1 = points_in_buffers) then
				in_select <= 0;
				ready <= '1';
			else
				in_select <= in_select + 1;
				ready <= '0';
			end if;

			-- Output buffer
			if (output_valid = '1') then
				if (out_select + 1 = points_in_buffers) then
					out_select <= 0;
					valid <= '1';
				else
					out_select <= out_select + 1;
					valid <= '0';
				end if;
			end if;
		end if;
	end process;

	pipeline_in <= data_in((in_select + 1) * point_depth - 1 downto in_select * point_depth);

	data_out((out_select + 1) * point_depth - 1 downto out_select * point_depth) <= pipeline_out;

	PIPE:
	entity work.pipeline port map(
		clk			=> clk,
		enable		=> enable,
		point_in	=> pipeline_in,
		point_out	=> pipeline_out
	);

end arch;
