library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity loader is
	Generic (
		offset		: integer
	);
	Port (
		clk			: in std_logic;
		pixel_base	: in integer;
		buf			: in std_logic;
		point_in	: in point;

		buf0		: out dmatrix;
		buf1		: out dmatrix
	);
end loader;

architecture arch of loader is
	signal row	: integer;
	signal col	: integer;
begin

	T_OFFSET:
	entity work.timing_offset
	generic map(
		offset	=> offset
	)
	port map(
		pixel_base	=> pixel_base,

		row			=> row,
		col			=> col
	);

	process(clk) is
	begin
		if (rising_edge(clk)) then
			-- Load input into buffer
			if buf = '0' then
				buf1(row)(col) <= std_logic_vector(to_signed(to_integer(unsigned(point_in)) - 127, 2 * point_depth));
			else
				buf0(row)(col) <= std_logic_vector(to_signed(to_integer(unsigned(point_in)) - 127, 2 * point_depth));
			end if;
		end if;
	end process;
end arch;
