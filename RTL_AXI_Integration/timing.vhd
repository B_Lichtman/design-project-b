library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity timing is
	Port (
		clk			: in std_logic;

		enable		: in std_logic;

		buf			: out std_logic;

		pixel_base	: out integer := 0
	);
end timing;

architecture arch of timing is
	signal sig_buf		: std_logic := '0';

	signal sig_pixel_base	: integer := 0;
begin
	process(clk) is
	begin
		if (rising_edge(clk)) then
			if (enable = '1') then
				if(sig_pixel_base + 1 >= block_size * block_size) then
					sig_pixel_base <= 0;
					sig_buf <= not sig_buf;
				else
					sig_pixel_base <= sig_pixel_base + 1;
				end if;
			end if;
		end if;
	end process;

	buf <= sig_buf;

	pixel_base <= sig_pixel_base;
end arch;
