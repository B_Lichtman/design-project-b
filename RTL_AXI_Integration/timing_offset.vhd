library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity timing_offset is
	Generic (
		offset		: integer
	);
	Port (
		pixel_base	: in integer;

		--pixel		: out integer;
		row			: out integer := 0;
		col			: out integer := 0
	);
end timing_offset;

architecture arch of timing_offset is
	signal pixel	: integer	:= 0;
begin
	pixel <= (pixel_base - offset) mod (block_size * block_size);

	row <= pixel / block_size;
	col <= pixel mod block_size;
end arch;
