library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity quantise is
	Generic (
		offset		: integer
	);
	Port (
		clk			: in std_logic;
		pixel_base	: in integer;
		matrix_in	: in dmatrix;

		o			: out dmatrix
	);
end quantise;

architecture arch of quantise is
	signal row	: integer;
	signal col	: integer;

	signal quantised	: dpoint;
begin

	T_OFFSET:
	entity work.timing_offset
	generic map(
		offset	=> offset
	)
	port map(
		pixel_base	=> pixel_base,

		row			=> row,
		col			=> col
	);

	quantised <= std_logic_vector(shift_right(signed(matrix_in(row)(col)), QMATRIX(row)(col)));

	process(clk) is
	begin
		if (rising_edge(clk)) then
			o(row)(col) <= quantised;
		end if;
	end process;

end arch;
