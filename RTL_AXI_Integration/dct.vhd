library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

-- The output of this will be stored row major so it can be used as the LHS of another matrix multiply

entity dct is
	Generic (
		offset		: integer
	);
	Port (
		clk			: in std_logic;
		pixel_base	: in integer;
		matrix_in	: in dmatrix;

		o			: out dmatrix
	);
end dct;

architecture arch of dct is
	signal buf	: std_logic	:= '0';

	signal buf0	: dmatrix;
	signal buf1	: dmatrix;

	signal intermediate	: dmatrix;

	signal out_a	: dpoint;
	signal row_a	: integer;
	signal col_a	: integer;

	signal out_b	: dpoint;
	signal row_b	: integer;
	signal col_b	: integer;
begin

	FIRST_MULT:
	entity work.matrix_multipy
	generic map(
		offset		=> offset
	)
	port map(
		clk			=> clk,
		pixel_base	=> pixel_base,
		matrix_l	=> DCT_TABLE,
		matrix_r	=> matrix_in,

		o			=> out_a,
		o_row		=> row_a,
		o_col		=> col_a
	);

	-- For double-buffering
	intermediate <= buf0 when buf = '0' else buf1;

	process(clk) is
	begin
		if (rising_edge(clk)) then
			-- Load into buffer
			if (buf = '0') then
				buf1(row_a)(col_a) <= out_a;
			else
				buf0(row_a)(col_a) <= out_a;
			end if;

			if(row_a = block_size - 1 and col_a = block_size - 1) then
				buf <= not buf;
			end if;
		end if;
	end process;

	SECOND_MULT:
	entity work.matrix_multipy
	generic map(
		offset		=> offset + 11
	)
	port map(
		clk			=> clk,
		pixel_base	=> pixel_base,
		matrix_l	=> intermediate,
		matrix_r	=> IDCT_TABLE,

		o			=> out_b,
		o_row		=> row_b,
		o_col		=> col_b
	);

	process(clk) is
	begin
		if (rising_edge(clk)) then
			o(row_b)(col_b) <= out_b;
		end if;
	end process;

end arch;
