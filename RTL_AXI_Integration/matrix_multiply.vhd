library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

-- The output of this will be stored row major so it can be used as the LHS of another matrix multiply

entity matrix_multipy is
	Generic (
		offset		: integer
	);
	Port (
		clk			: in std_logic;
		pixel_base	: in integer;
		matrix_l	: in dmatrix;
		matrix_r	: in dmatrix;

		o			: out dpoint;
		o_row		: out integer;
		o_col		: out integer
	);
end matrix_multipy;

architecture arch of matrix_multipy is
	signal row_a	: integer;
	signal col_a	: integer;

	signal row_b	: integer;
	signal col_b	: integer;

begin

	T_OFFSET_A:
	entity work.timing_offset
	generic map(
		offset	=> offset
	)
	port map(
		pixel_base	=> pixel_base,

		row			=> row_a,
		col			=> col_a
	);

	T_OFFSET_B:
	entity work.timing_offset
	generic map(
		offset	=> offset + 10
	)
	port map(
		pixel_base	=> pixel_base,

		row			=> row_b,
		col			=> col_b
	);

	CONVOLVE:
	entity work.convolve port map(
			clk			=> clk,
			line_left	=> matrix_l(row_a),
			line_right	=> matrix_r(col_a),

			o			=> o
	);

	o_row <= row_b;
	o_col <= col_b;

end arch;
