library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.my_library.all;

entity outp is
	Generic (
		offset		: integer
	);
	Port (
		clk			: in std_logic;
		pixel_base	: in integer;
		matrix_in	: in dmatrix;

		o			: out point
	);
end outp;

architecture arch of outp is
	signal row	: integer;
	signal col	: integer;

	signal rebased	: dpoint;
begin

	T_OFFSET:
	entity work.timing_offset
	generic map(
		offset	=> offset
	)
	port map(
		pixel_base	=> pixel_base,

		row			=> row,
		col			=> col
	);

	process(clk) is
	begin
		if (rising_edge(clk)) then
			-- Select output from buffer
			rebased <= std_logic_vector(to_signed(to_integer(unsigned(matrix_in(row)(col))) + 127, 2 * point_depth));
		end if;
	end process;

	o <= rebased(point_depth - 1 downto 0);

end arch;
